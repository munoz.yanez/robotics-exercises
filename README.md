![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

---

This is the main page for the textbook of robotics subject

Yo can browse the files in the repo:
  - [Book](docs/README.md)

Or use the webpage version:
  - [Webpage](https://robotics-exercises-munoz-yanez-961bf9c324bce3771290e214474b2822.gitlab.io/)


Also the pdf file will be available soon.
